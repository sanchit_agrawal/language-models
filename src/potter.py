from nltk import word_tokenize
import re
import numpy as np
import tensorflow as tf
from random import shuffle
import time
from nltk.corpus import brown

# words = [clean(w) for w in brown.words()]

def clean(w):
	w = w.lower()
	try:
		f = float(w)
		return 'num_tok'
	except:
		return w

with open('HP3.txt', 'r') as f:
	text = f.read()
words = [clean(w) for w in word_tokenize(text)]
word_count = {}
for word in words:
	if word in word_count:
		word_count[word] += 1
	else:
		word_count[word] = 1
words = [w for w in words if word_count[w] >= 2]
word_set = set(words)
print 'Vocabulary size:', len(word_set)
word_dict = {w:i for i, w in enumerate(sorted(word_set))}
inverse_dict = {i:w for i, w in enumerate(sorted(word_set))}
tokens = [word_dict[w] for w in words]
train_tokens, valid_tokens, test_tokens = tokens[:int(0.8*len(tokens))], tokens[int(0.8*len(tokens)):int(0.9*len(tokens))], tokens[int(0.9*len(tokens)):]
print '# tokens (train, valid, test):', len(train_tokens), len(valid_tokens), len(test_tokens)
# TENSORFLOW BEGINS

# TODO: Fill sentences with -1 when > seq_lengths
class LanguageModel():
	
	def __init__(self, args):
		self.init_sample = False
		batch_size = args['batch_size']
		max_time = args['max_time']
		embedding_size = args['embedding_size']
		vocab_size = args['vocab_size']
		n_steps = args['n_steps']
		self.args = args
		self.sequences = tf.placeholder(tf.int32, [batch_size, max_time])
		self.input_keep_prob = tf.placeholder_with_default(1.0, shape = ())
		self.output_keep_prob = tf.placeholder_with_default(1.0, shape = ())
		self.embedding_matrix = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0))
		self.lstm_cell = tf.nn.rnn_cell.LSTMCell(embedding_size, use_peepholes = True, initializer = tf.contrib.layers.xavier_initializer())
		self.lstm_cell = tf.nn.rnn_cell.MultiRNNCell([self.lstm_cell]*3)
		self.lstm_cell = tf.nn.rnn_cell.OutputProjectionWrapper(self.lstm_cell, vocab_size)
		# self.lstm_cell = tf.nn.rnn_cell.DropoutWrapper(self.lstm_cell, input_keep_prob = self.input_keep_prob, output_keep_prob = self.output_keep_prob, seed = int(time.time()*10000))
		self.word_embeddings = tf.nn.embedding_lookup(self.embedding_matrix, self.sequences)
		self.word_embeddings = tf.unpack(self.word_embeddings, axis = 1)
		self.outputs, _ = tf.nn.seq2seq.rnn_decoder(
			self.word_embeddings,
			self.lstm_cell.zero_state(batch_size, tf.float32),
			self.lstm_cell
			# loop_function = lambda prev, _ : tf.nn.embedding_lookup(self.embedding_matrix, tf.argmax(prev, axis = 1))
			# loop_function = lambda prev, _ : tf.nn.embedding_lookup(self.embedding_matrix, tf.squeeze(tf.multinomial(prev, 1), [1]))
		)
		self.targets = tf.unpack(self.sequences, axis = 1)
		self.cross_entropy = tf.nn.seq2seq.sequence_loss(self.outputs[:-1], self.targets[1:], [[1.0]*batch_size]*(max_time-1))
		self.train_step = tf.train.AdamOptimizer().minimize(self.cross_entropy)
		# Sampling ops
		tf.get_variable_scope().reuse_variables()
		self.sample_input = tf.placeholder(tf.int32, [1, n_steps])
		self.sample_embeddings = tf.nn.embedding_lookup(self.embedding_matrix, self.sample_input)
		self.sample_embeddings = tf.unpack(self.sample_embeddings, axis = 1)

		def weighted_average(prev, i):
			temp_probs = tf.nn.softmax(prev)
			return tf.matmul(temp_probs, self.embedding_matrix)

		self.sample_outputs, _ = tf.nn.seq2seq.rnn_decoder(
			self.sample_embeddings,
			self.lstm_cell.zero_state(1, tf.float32),
			self.lstm_cell,
			loop_function = lambda prev, _ : tf.nn.embedding_lookup(self.embedding_matrix, tf.squeeze(tf.multinomial(prev, 1), [1]))
			# loop_function = lambda prev, _ : tf.nn.embedding_lookup(self.embedding_matrix, tf.argmax(prev, axis = 1))
			# loop_function = weighted_average
		)
		self.sample_outputs = tf.pack(self.sample_outputs, axis = 1)
		self.sample_outputs = tf.squeeze(self.sample_outputs)
		self.predicted_words = tf.argmax(self.sample_outputs, axis = 1)
		# Initialization
		self.init = tf.global_variables_initializer()
		# self.saver = tf.train.Saver()
	
	def test(self, session, batch):
		return session.run(self.cross_entropy, feed_dict = {self.sequences: batch})

	def train(self, session, batch, input_keep_prob = 1.0, output_keep_prob = 1.0):
		loss, _ = session.run([self.cross_entropy, self.train_step], feed_dict = {self.sequences: batch, self.input_keep_prob: input_keep_prob, self.output_keep_prob: output_keep_prob})
		return loss

	def sample(self, session, start_word):
		words = session.run(self.predicted_words, feed_dict = {self.sample_input: [[start_word]*self.args['n_steps']]})
		return [start_word] + list(words)

session = tf.Session()
epochs = 50
keep_prob = 0.9
args = {}
args['batch_size'] = batch_size = 64
args['embedding_size'] = 100
args['vocab_size'] = len(word_set)
args['max_time'] = max_length = 10
args['n_steps'] = 200
model = LanguageModel(args)
session.run(model.init)
train_phrases = [train_tokens[i:i+max_length] for i in range(0, len(train_tokens)-max_length, max_length)]
valid_phrases = [valid_tokens[i:i+max_length] for i in range(0, len(valid_tokens)-max_length, max_length)]
test_phrases = [test_tokens[i:i+max_length] for i in range(0, len(test_tokens)-max_length, max_length)]
train_batches = [train_phrases[i:i+batch_size] for i in range(0, len(train_phrases)-batch_size, batch_size)]
valid_batches = [valid_phrases[i:i+batch_size] for i in range(0, len(valid_phrases)-batch_size, batch_size)]
test_batches = [test_phrases[i:i+batch_size] for i in range(0, len(test_phrases)-batch_size, batch_size)]
# shuffle(batches)
for i in range(epochs):
	print 'Epoch', i
	total_loss = 0
	shuffle(train_batches)
	for j, batch in enumerate(train_batches):
		loss = model.train(session, batch, keep_prob, keep_prob)
		total_loss += loss
		if j % (len(train_batches)/10) == 0:
			print (j / (len(train_batches)/10))*10, r'%'
	print 'Avg train perplexity:', np.e**(total_loss / len(train_batches))
	print 'Avg valid perplexity:', np.e**np.mean([model.test(session, batch) for batch in valid_batches])
	print 'Avg test perplexity:', np.e**np.mean([model.test(session, batch) for batch in test_batches])
	preds = model.sample(session, word_dict['the'])
	for n in preds:
		print inverse_dict[n],
	print ''
	# model.saver.save(session, 'trumper')